package JUnit.Junit09_Geometria;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import JUnit.Junit09_Geometria.dto.*;;

public class AppTest 
{
	Geometria testear=new Geometria();
	
	@Test
	public void testCons() {
		// TODO Auto-generated method stub
		int resultado = testear.areacuadrado(4);
		int esperado = 16;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaCuadrado() {
		// TODO Auto-generated method stub
		int resultado = testear.areacuadrado(4);
		int esperado = 16;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaCirculo() {
		// TODO Auto-generated method stub
		double resultado = testear.areaCirculo(2);
		double esperado= 12.5664;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaTriangulo() {
		int resultado = testear.areatriangulo(2, 5);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaRectangulo() {
		int resultado = testear.arearectangulo(3, 4);
		int esperado = 12;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaPentagono() {
		int resultado = testear.areapentagono(5, 2);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	
	
	@Test
	public void testAreaRombo() {	
		int resultado = testear.arearombo(8, 8);
		int esperado = 32;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaRomboide() {
		int resultado = testear.arearomboide(7, 6);
		int esperado = 42;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testAreaTrapecio() {
		int resultado = testear.areatrapecio(1, 9, 4);
		int esperado = 20;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testFigura() {
		String resultado = testear.figura(1);
		String esperado = "Cuadrado";
		assertEquals(esperado, resultado);
		//Para no hacer el ejemplo tan pesado, lo he puesto asi para comprobar.
		assertEquals("Circulo", testear.figura(2));
		assertEquals("Triangulo", testear.figura(3));
		assertEquals("Rectangulo", testear.figura(4));
		assertEquals("Pentagono",testear.figura(5));
		assertEquals("Rombo", testear.figura(6));
		assertEquals("Romboide", testear.figura(7));
		assertEquals("Trapecio", testear.figura(8));
		assertEquals("Default", testear.figura(12));
	}
	
	
	@Test
	void testSetId() {
		testear.setId(5);
		
		int resultado = testear.getId();
		int esperado = 5;
		assertEquals(esperado, resultado);
	}

	@Test
	void testSetGetNom() {
		testear.setNom("Triangulo");
		
		String resultado = testear.getNom();
		String esperado = "Triangulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testSetGetArea() {
		testear.setArea(10);
		
		double resultado = testear.getArea();
		double esperado = 1;
		assertEquals(esperado, resultado);
	}

	

}
